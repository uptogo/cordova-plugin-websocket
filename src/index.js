const plugin = require('cordova')

const enroll = []

const router = result => {
  const { channel, content, subject: action } = JSON.parse(result)
  enroll.filter(
    ({ subject }) => subject === action
  ).forEach(
    ({ execute }) => execute(channel, content)
  )
}

const gather = misfit => {
  console.error(misfit)
}

const finish = () => {
  plugin.exec(router, gather, 'Interface', 'finish')
}

const listen = thread => {
  plugin.exec(router, gather, 'Interface', 'listen', [thread])
}

module.exports.enroll = enroll
module.exports.finish = finish
module.exports.listen = listen
