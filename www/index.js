"use strict";

var plugin = require('cordova');

var enroll = [];

var router = function router(result) {
  var _JSON$parse = JSON.parse(result),
      channel = _JSON$parse.channel,
      content = _JSON$parse.content,
      action = _JSON$parse.subject;

  enroll.filter(function (_ref) {
    var subject = _ref.subject;
    return subject === action;
  }).forEach(function (_ref2) {
    var execute = _ref2.execute;
    return execute(channel, content);
  });
};

var gather = function gather(misfit) {
  console.error(misfit);
};

var finish = function finish() {
  plugin.exec(router, gather, 'Interface', 'finish');
};

var listen = function listen(thread) {
  plugin.exec(router, gather, 'Interface', 'listen', [thread]);
};

module.exports.enroll = enroll;
module.exports.finish = finish;
module.exports.listen = listen;