package uptogo.stream.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.apache.cordova.CallbackContext;

import uptogo.stream.plugin.Result;
import uptogo.stream.service.Definition;

public class Receiver extends BroadcastReceiver {
  private CallbackContext callback;
  private String channel;

  public String getChannel() {
    return this.channel;
  }

  public Receiver setChannel(String channel) {
    this.channel = channel;
    return this;
  }

  public CallbackContext getCallback() {
    return this.callback;
  }

  public Receiver setCallback(CallbackContext callback) {
    this.callback = callback;
    return this;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    if (this.getChannel() == null || this.getChannel().equals(intent.getStringExtra(Definition.EXTRA_CHANNEL))) {
      this.getCallback().sendPluginResult(new Result(intent.getStringExtra(Definition.EXTRA_MESSAGE)));
    }
    this.setChannel(intent.getStringExtra(Definition.EXTRA_CHANNEL));
  }
}
