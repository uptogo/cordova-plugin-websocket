package uptogo.stream.broadcast;

import org.json.JSONException;
import org.json.JSONObject;

public class Message {
  private String channel;
  private String content;
  private String subject;

  public String getChannel() {
    return this.channel;
  }

  public Message setChannel(String channel) {
    this.channel = channel;
    return this;
  }

  public String getContent() {
    return this.content;
  }

  public Message setContent(String content) {
    this.content = content;
    return this;
  }

  public String getSubject() {
    return this.subject;
  }

  public Message setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  public String encode() throws JSONException {
    return new JSONObject()
      .put("channel", this.getChannel())
      .put("content", this.getContent())
      .put("subject", this.getSubject())
      .toString();
  }

  public Message decode(String encoded) throws JSONException {
    JSONObject decoded = new JSONObject(encoded);
    return this
      .setChannel(decoded.optString("channel"))
      .setContent(decoded.optString("content"))
      .setSubject(decoded.optString("subject"));
  }
}
