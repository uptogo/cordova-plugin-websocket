package uptogo.stream.broadcast;

import android.content.IntentFilter;

import uptogo.stream.service.Definition;

public class Filter extends IntentFilter {
  public Filter() {
    super();
    this.addAction(Definition.ACTION_FAILED);
    this.addAction(Definition.ACTION_FINISH);
    this.addAction(Definition.ACTION_LISTEN);
    this.addAction(Definition.ACTION_NOTICE);
    this.addAction(Definition.ACTION_REPAIR);
  }
}
