package uptogo.stream.plugin;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import uptogo.stream.broadcast.Filter;
import uptogo.stream.broadcast.Message;
import uptogo.stream.broadcast.Receiver;
import uptogo.stream.service.Connection;
import uptogo.stream.service.Definition;

public class Interface extends CordovaPlugin {
  private Connection connection;
  private Receiver receiver;

  public Connection getConnection() {
    return this.connection;
  }

  public Interface setConnection(Connection connection) {
    this.connection = connection;
    return this;
  }

  public Receiver getReceiver() {
    return this.receiver;
  }

  public Interface setReceiver(Receiver receiver) {
    this.receiver = receiver;
    return this;
  }

  private Interface registerReceiver(CallbackContext callback) {
    if (this.getReceiver() == null) {
      cordova.getActivity().getApplicationContext().registerReceiver(
        this.setReceiver(new Receiver().setCallback(callback)).getReceiver(),
        new Filter()
      );
    }
    return this;
  }

  private Interface unregisterReceiver() {
    if (this.getReceiver() != null) {
      try {
        this.getReceiver().onReceive(
          cordova.getActivity(),
          new Intent()
            .setAction(
              Definition.ACTION_FINISH
            )
            .putExtra(
              Definition.EXTRA_CHANNEL,
              this.getReceiver().getChannel()
            )
            .putExtra(
              Definition.EXTRA_MESSAGE,
              new Message()
                .setSubject(Definition.ACTION_FINISH)
                .encode()
            )
        );
      } catch (Exception exception) {
        Log.e(this.getClass().getName(), exception.getMessage());
      }
      cordova.getActivity().getApplicationContext().unregisterReceiver(this.getReceiver());
      this.setReceiver(null);
    }
    return this;
  }

  private Interface bindService(String channel) {
    if (this.getConnection() == null) {
      cordova.getActivity().getApplicationContext().bindService(
        new Intent(
          cordova.getActivity(),
          Definition.class
        ).putExtra(
          Definition.EXTRA_CHANNEL,
          channel
        ),
        this.setConnection(new Connection()).getConnection(),
        Context.BIND_AUTO_CREATE
      );
    } else if (!channel.equals(this.getReceiver().getChannel())) {
      this.unbindService().bindService(channel);
    }
    return this;
  }

  private Interface unbindService() {
    if (this.getConnection() != null) {
      cordova.getActivity().getApplicationContext().unbindService(this.getConnection());
      this.setConnection(null);
    }
    return this;
  }

  @Override
  public boolean execute(String action, JSONArray arguments, CallbackContext callback) throws JSONException {
    switch (action) {
      case Definition.ACTION_FINISH:
        this.unregisterReceiver().unbindService();
        break;
      case Definition.ACTION_LISTEN:
        this.registerReceiver(callback).bindService(arguments.getString(0));
        break;
    }
    return true;
  }

  @Override
  public void onDestroy() {
    this.unregisterReceiver().unbindService();
  }
}
