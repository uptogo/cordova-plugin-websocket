package uptogo.stream.plugin;

import org.apache.cordova.PluginResult;

public class Result extends PluginResult {
  public Result(String message) {
    super(Status.OK, message);
    this.setKeepCallback(true);
  }
}
