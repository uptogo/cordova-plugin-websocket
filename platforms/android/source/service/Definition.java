package uptogo.stream.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import uptogo.stream.websocket.Listener;

public class Definition extends Service {
  public static final String ACTION_FAILED = "failed";
  public static final String ACTION_FINISH = "finish";
  public static final String ACTION_LISTEN = "listen";
  public static final String ACTION_NOTICE = "notice";
  public static final String ACTION_REPAIR = "repair";
  public static final String EXTRA_CHANNEL = "channel";
  public static final String EXTRA_MESSAGE = "message";

  private OkHttpClient client;
  private Listener listener;
  private WebSocket socket;

  public OkHttpClient getClient() {
    return this.client;
  }

  public Definition setClient(OkHttpClient client) {
    this.client = client;
    return this;
  }

  public Listener getListener() {
    return this.listener;
  }

  public Definition setListener(Listener listener) {
    this.listener = listener;
    return this;
  }

  public WebSocket getSocket() {
    return this.socket;
  }

  public Definition setSocket(WebSocket socket) {
    this.socket = socket;
    return this;
  }

  public void listen(String channel) throws NullPointerException {
    this.setListener(
      this.getListener() != null ? this.getListener() : new Listener().setContext(this).setPrimary(true)
    ).setSocket(
      this.getClient().newWebSocket(
        new Request
          .Builder()
          .url("wss://stream.logxc.com.br".concat(channel))
          .header("user-agent", Build.MANUFACTURER.concat(" ").concat(Build.MODEL))
          .build(),
        this.getListener()
      )
    );
  }

  @Override
  public void sendBroadcast(Intent intent) {
    super.sendBroadcast(intent);
    if (intent.getAction() == ACTION_FAILED) {
      try {
        Thread.sleep(10000);
        this.listen(intent.getStringExtra(EXTRA_CHANNEL));
      } catch (Exception exception) {
        Log.e(this.getClass().getName(), exception.getMessage());
      }
    }
  }

  @Override
  public void onCreate() {
    Log.i(this.getClass().getName(), "The service was created.");
    this.setClient(
      new OkHttpClient
        .Builder()
        .pingInterval(10, TimeUnit.SECONDS)
        .build()
    );
  }

  @Override
  public void onDestroy() {
    Log.i(this.getClass().getName(), "The service was destroyed.");
    this.getClient().dispatcher().executorService().shutdown();
    this.getClient().connectionPool().evictAll();
    this.setClient(null);
  }

  @Override
  public IBinder onBind(Intent intent) {
    Log.i(this.getClass().getName(), "The service has been connected.");
    this.listen(intent.getStringExtra(EXTRA_CHANNEL));
    return new Binder();
  }

  @Override
  public boolean onUnbind(Intent intent) {
    Log.i(this.getClass().getName(), "The service has been disconnected.");
    this.getSocket().close(1000, null);
    this.setSocket(null);
    return false;
  }
}
