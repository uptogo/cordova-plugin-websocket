package uptogo.stream.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public class Connection implements ServiceConnection {
  @Override
  public void onServiceConnected(ComponentName name, IBinder service) {
    Log.i(this.getClass().getName(), "The service connection has been established.");
  }

  @Override
  public void onServiceDisconnected(ComponentName name) {
    Log.i(this.getClass().getName(), "The service connection has been terminated.");
  }
}
