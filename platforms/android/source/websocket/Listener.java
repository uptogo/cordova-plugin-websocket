package uptogo.stream.websocket;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import uptogo.stream.broadcast.Message;
import uptogo.stream.service.Definition;

public class Listener extends WebSocketListener {
  private Context context;
  private Boolean primary;

  public Context getContext() {
    return this.context;
  }

  public Listener setContext(Context context) {
    this.context = context;
    return this;
  }

  public Boolean getPrimary() {
    return this.primary;
  }

  public Listener setPrimary(Boolean primary) {
    this.primary = primary;
    return this;
  }

  @Override
  public void onOpen(WebSocket socket, Response response) {
    Log.i(this.getClass().getName(), "The WebSocket has been opened.");
    try {
      this.getContext().sendBroadcast(
        new Intent()
          .setAction(
            this.getPrimary() ? Definition.ACTION_LISTEN : Definition.ACTION_REPAIR
          )
          .putExtra(
            Definition.EXTRA_CHANNEL,
            socket.request().url().encodedPath()
          )
          .putExtra(
            Definition.EXTRA_MESSAGE,
            new Message()
              .setSubject(
                this.getPrimary() ? Definition.ACTION_LISTEN : Definition.ACTION_REPAIR
              )
              .encode()
          )
      );
    } catch (Exception exception) {
      Log.e(this.getClass().getName(), exception.getMessage());
    } finally {
      this.setPrimary(true);
    }
  }

  @Override
  public void onMessage(WebSocket socket, String message) {
    Log.i(this.getClass().getName(), "The WebSocket received a message.");
    try {
      this.getContext().sendBroadcast(
        new Intent()
          .setAction(
            Definition.ACTION_NOTICE
          )
          .putExtra(
            Definition.EXTRA_CHANNEL,
            socket.request().url().encodedPath()
          )
          .putExtra(
            Definition.EXTRA_MESSAGE,
            new Message()
              .setChannel(socket.request().url().encodedPath())
              .setContent(message)
              .setSubject(Definition.ACTION_NOTICE)
              .encode()
          )
      );
    } catch (Exception exception) {
      Log.e(this.getClass().getName(), exception.getMessage());
    }
  }

  @Override
  public void onClosing(WebSocket socket, int code, String reason) {
    Log.i(this.getClass().getName(), "The WebSocket will be closed.");
    socket.close(code, reason);
  }

  @Override
  public void onClosed(WebSocket socket, int code, String reason) {
    Log.i(this.getClass().getName(), "The WebSocket has been closed.");
  }

  @Override
  public void onFailure(WebSocket socket, Throwable throwable, Response response) {
    Log.e(this.getClass().getName(), "The WebSocket was failed.");
    try {
      this.getContext().sendBroadcast(
        new Intent()
          .setAction(
            Definition.ACTION_FAILED
          )
          .putExtra(
            Definition.EXTRA_CHANNEL,
            socket.request().url().encodedPath()
          )
          .putExtra(
            Definition.EXTRA_MESSAGE,
            new Message()
              .setSubject(Definition.ACTION_FAILED)
              .encode()
          )
      );
    } catch (Exception exception) {
      Log.e(this.getClass().getName(), exception.getMessage());
    } finally {
      this.setPrimary(false);
    }
  }
}
